package BuscaEmLargura;

import java.util.ArrayList;

import BuscaEmLargura.Keyboard;
import BuscaEmLargura.Aresta;
import BuscaEmLargura.Grafo;
import BuscaEmLargura.Vertice;

public class BuscaLargura {
	

	public static void main(String arg[]) {

		Grafo inicial = new Grafo();
		Grafo resultado = new Grafo();

		
		Aresta arestaAux;
		Vertice verticeAux1, verticeAux2;
		int opcao = 5, peso;
		String origem , destino;
		
		while(opcao!=0){
			System.out.println("1 - Add vertices e Arestas");
			System.out.println("2 - Imprimir Grafo Dado");
			System.out.println("3 - Obter arvore de Busca em Profundidade");
			System.out.println("4 - Obter arvore de Busca em Largura");
			System.out.println("0 - Sair");
			
			opcao = Keyboard.readInt();
			
			//dando um reset no grafo de resultado
			resultado.clearLists();
			
			//limpando verificadores booleanos
			inicial.limparArestaVisitada();
			inicial.limparVerticeVisitado();
			
			switch (opcao){
			case 1:
				System.out.println("Digite o Vertice Origem: ");
				origem = Keyboard.readString();
				System.out.println("Digite o Vertice Destino: ");
				destino = Keyboard.readString();
				System.out.println("Digite o Peso da Aresta: ");
				peso = Keyboard.readInt();
				inicial.addAresta(origem,destino,peso);
				
				inicial.imprimeArvore();
				break;
			case 2:
				inicial.imprimeArvore();
				break;				
			case 3:
				//Algoritmo de Busca em Profundidade
				System.out.println("Digite o Vertice Origem: ");
				origem = Keyboard.readString();
				System.out.println("Digite o Vertice Destino: ");
				destino = Keyboard.readString();
				resultado.setArestas(inicial.buscaEmProfundidade(origem,destino));
				
				resultado.imprimeArvore();
				break;
			case 4:
				//Algoritmo de Busca em Largura
				System.out.println("Digite o Vertice Origem: ");
				origem = Keyboard.readString();
//				System.out.println("Digite o Vertice Destino: ");
//				destino = Keyboard.readString();
				resultado.setArestas(inicial.buscaEmLargura(origem));
				
				resultado.imprimeArvore();
				break;
			}
		}
		System.out.println("Obrigado Por Usar o Programa =D !");
	}
}